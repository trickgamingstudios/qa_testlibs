/* eslint-disable import/no-extraneous-dependencies  */
import { When, Given, Then } from 'cucumber';
import { pages } from '../pages/pages';

Given('A user', async () => {
  await pages.login.navigate();
});
When('I enter my username {} and password', async (username) => {
     await pages.login.login(username);
});
Then('I view the stagecoach feed', async () => {
    await pages.login.loginValidate();
});

