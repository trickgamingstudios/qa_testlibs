import { testController } from '../support/world'
import { select } from '../support/utils'
import { ClientFunction } from 'testcafe';

const dotenv = require('dotenv');

export class Login {
  constructor () {
    dotenv.config();
    this.url = process.env.URL_HOST
    this.urlHome = process.env.URL_HOST + `apps/home`
  }
//LOGIN SELECTORS
  usernameImput () {
    return select('#username')
  }
  passwordImput () {
    return select('#password')
  }
  stagecoachIcon () {
    return select ('.logo > a:nth-child(1) > svg:nth-child(1) > path:nth-child(3)').exists
  }
  loginButton () {
    return select('button.btn-main:nth-child(1)');
  }
  welcomeText () {
    return select('div.fadeInUp:nth-child(1) > h1:nth-child(1)').innerText;
  }
  async navigate () {
    await testController.navigateTo(this.url)
  }
  async navigateHome () {
    dotenv.config();
    this.urlHome = process.env.URL_HOST + `/home`;
    console.log(this.urlHome);
    const getLocation = ClientFunction(() => document.location.href).with({ boundTestRun: testController });
    await testController.expect(getLocation()).contains(this.urlHome);
  }
  async login (username) {
    await testController
      .click(this.usernameImput())
      .typeText(this.usernameImput(), username, { paste: false })
      .click(this.passwordImput())
      .typeText(this.passwordImput(), process.env.PASSWORD, { paste: true })
      .click(this.loginButton());
  }
  async loginValidate () {
    const welcomeTitle= this.welcomeText();
    const stagecoachIcon = this.stagecoachIcon();
    await testController
      .expect(stagecoachIcon).ok('El titulo de stagecoach se visualiza correctamente', { allowUnawaitedPromise: false })
      .expect(welcomeTitle).eql("Welcome!");
    }
}
