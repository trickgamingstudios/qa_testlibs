Feature: WEB - Login Stagecoach

    Scenario: [U000001] As a user I want to sign in my Stagecoach account to interact with the content I like. 

        Given A user
        When I enter my username automationtest and password
        Then I view the stagecoach feed

