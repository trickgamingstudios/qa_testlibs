Feature: Categories Routes
  In order manage directory
  As a QA Automation
  I want to make sure CRUD operations through REST API works fine

  Scenario Outline: retrive all the categories 
    Given A account <request>
    When I send POST request to /get-stage-categories
    Then I get response code 200

    Examples:
      | request                 |
      | { "server-key": "40762726" } |


