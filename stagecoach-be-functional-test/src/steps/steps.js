const {Given, When, Then, AfterAll, After, And} = require('cucumber');
const assert = require('assert').strict
const restHelper = require('../util/restHelper');
const {makeid} = require('../util/util');

Given('A account {}', function (request) {
    console.log(request);
    this.context['request'] = request;
});


When('I send POST request', async function (path) {
    this.context['response'] = await restHelper.postData(`${process.env.SERVICE_URL}`, this.context['request']);
});


Then('I get response code {int}', async function (code) {
    assert.equal(this.context['response'].status, code);
});

